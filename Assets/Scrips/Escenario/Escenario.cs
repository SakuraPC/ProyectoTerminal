﻿
//---->Clase encargada del control del escenario donde la grafica es construida

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Ejes;
using Puntos;
using GraficaBarra;

public class Escenario : MonoBehaviour {

    private ejes e;
    private Scatter scater;
    private Barras barra;
    private Superficie super;
    private string[,] data;
    private bool inFirst = true;
    private WWW w;

    // Al iniciar el objeto...
    void Start () {
        //Peticion al servidor
        StartCoroutine(getDatos());
        //Esperá a una respuesta y comienza la construccion del escenario
        StartCoroutine(DoLast());
    }

    //Peticion al servidor
    IEnumerator getDatos()
    {
        //Bandera que nos indica si la peticion ya fue contestada
        inFirst = true;

        //Ruta donde se pide la descripcion de los datos de la grafica
        string ruta = TransferVariables.servidor+"graphic/" + TransferVariables.idGrafica.ToString();
        
        //Se realiza la peticion
        w = new WWW(ruta);
        yield return w;
        yield return new WaitForSeconds(1f);
        
        //Procedemos a parsear los datos recibidos para guardarlos
        extraeDatos(w.text);

        //Indicamos que la peticion ha sido completada
        inFirst = false;
    }

    void extraeDatos(string json)
    {
        //Convetirmos la respuesta a un JSON
        JSONObject jo = new JSONObject(json);

        //Matriz donde los datos de la grafica serán guardados
        string [,] datosAPI = new string[jo.Count + 1, jo[0].Count];
        
        //Lista donde el nombre de las variables será guardada
        string[] series = new string[jo[0].Count -1];

        //Coloca dentro de la matriz nombre de las variables
        for (int i = 0; i < jo.Count; i++)
        {
            datosAPI[i + 1, 0] = jo[i]["field1"].ToString().Replace("\"", "");
        }

        //El campo "field1" nos indica que la columna pertenece al nombre de las variables, 
        //pero dado que si el nombre de las variables son numericas, el campo "fiel1" aparece al final
        //y cuando son texto aparece al principio, esto es debido al codigo de la API.
        //Procedemos a recorrer las columnas para guardar el nombre de las series
        //ya que al ser texto plano no podemos distinguir si son texto o numericas
        int disp = jo[0].Count - 1, cont = 0, pos=0;
        while(disp>0)
        {
            if (jo[0].keys[cont].ToString().Replace("\"", "") != "field1")
            {
                series[pos] = jo[0].keys[cont].ToString().Replace("\"", "");
                pos++;
                disp--;
            }
            cont++;
        }
        
        //Agregamos las series a la matriz
        for (int j = 0; j < series.GetLength(0); j++)
        {
            datosAPI[0, j+1] = series[j];
        }
        
        //Ahora procedemos a guardar los datos numericos en la matriz
        for (int i = 1; i < jo.Count+1; i++)
        {
            for (int j = 0; j < series.GetLength(0); j++)
            {
                    datosAPI[i, j+1] = jo[i-1][series[j]].ToString().Replace("\"", "");
            }
        }

        //Guardamos los datos para que la funcion perteneciente a la grafica pueda tener acceso a el
        TransferVariables.rawGrafica= datosAPI;
    }

    //Una vez que los datos han sido guardados
    IEnumerator DoLast()
    {
        while (inFirst)
        {
            yield return new WaitForSeconds(0.1f);
        }

        //Obtenemos los datos guardados
        data = TransferVariables.rawGrafica;

        //Identificamos en que escena estamos actualemente, dependiendo de ella se crean los muros y ejes; ademas se ejecutan los
        //scrips que contruyen el tipo de grafica correspondiente
        switch (SceneManager.GetActiveScene().name)
        {
            case "barras":
                creaMuro(10, 0, 0, 10, 20, 5.2f);
                creaMuro(0, 10, 90, 10, 20, 5.2f);
                creaBase(20, 20);
                barra = new Barras(data);
                barra.main();
                //Obtenemos el valor maximo del conjunto de datos
                float max = barra.getMaximo();
                //Construimos los ejes indicandole el tipo de grafica que es
                e = new ejes(max, 1);
                break;

            case "scatter":
                creaMuro(15, 0, 0, 15, 30, 7.7f);
                creaMuro(0, 15, 90, 15, 30, 7.7f);
                creaBase(30, 30);
                scater = new Scatter(data);
                scater.main();
                //Obtenemos el valor maximo del conjunto de datos para cada uno de los ejes
                float[] maximos = scater.getMaximos();
                e = new ejes(maximos, 2);
                break;

            case "superficie":
                creaMuro(10, 0, 0, 10, 20, 5.2f);
                creaMuro(0, 10, 90, 10, 20, 5.2f);
                creaBase(20, 20);
                super = new Superficie(data);
                super.main();
                float maxi = super.getMaximo();
                e = new ejes(maxi, 1);
                break;
        }
        e.Iniciar();
    }

    //Crea muros de los ejes
    private void creaMuro(float posx, float posz, float roty, float scay, float scaz, float altura)
    {
        //Crea un cubo y le asigna un nombre
        GameObject tmp = GameObject.CreatePrimitive(PrimitiveType.Cube);
        tmp.gameObject.name = "MuroGrid";

        //Lo coloca en la posicion indicada, asi como el tamaño es ajustado y la rotacion
        tmp.transform.localPosition = new Vector3(posx, altura, posz);
        tmp.transform.localScale = new Vector3(.2f, scay, scaz);
        tmp.transform.localRotation = Quaternion.Euler(0, roty, 0);
    }

    //Crea la base donde se acentará el graficp
    private void creaBase(float sx, float sz)
    {
        GameObject tmp = GameObject.CreatePrimitive(PrimitiveType.Cube);
        tmp.gameObject.name = "Base";
        tmp.transform.localPosition = new Vector3(0, 0.1f, 0);
        tmp.transform.localScale = new Vector3(sx, .1f, sz);
    }
}

﻿
//---->Clase encargada crear los ejes y el grid de las graficas

using System.Globalization;
using UnityEngine;

namespace Ejes
{
    public class ejes
    {
        private int  tipo;
        private float max, val;
        private float[] maximos;

        //Constructor en caso detener un solo valor maximo
        public ejes(float m, int t )
        {
            max = m;
            tipo = t;
        }

        //Constructor en caso de tener 3 datos maximos, uno por eje
        public ejes(float[] m, int t)
        {
            maximos = m;
            tipo = t;
        }
        
        //Funcion principal
        public void Iniciar()
        {
            //Dependiendo del grafico se crean los ejes y lines auxiliares
            switch(tipo)
            {
                case 1:
                    ejesBarras();
                    break;
                case 2:
                    ejesScatter();
                    break;
            }
        }

        //Ejes para la grafica de barras
        private void ejesBarras()
        {
            //Se crean los ejes Y
            creaEjesY(-10, 5.2f,10, 5);
            creaEjesY(10, 5.2f,-10, 5);

            //Se crean las etiquetas
            creaLabel( .2f, max, 1,0, -10, 10, 45);
            creaLabel(.2f, max, 1,1,10, -10, 135);

            //Se crean lineas auxiliares a la lectura
            creaGrid(1, .2f, 9.8f, 0, 90, 0, 10, 1);
            creaGrid(1, .2f, 0, 9.8f, 90, 90, 10, 1);
        }

        private void ejesScatter()
        {
            //Creamos las etiquetas en los tres ejes
            creaLabel(.2f, maximos[1], 1.5f, 0, -15, 15, 45);
            creaLabel(.2f, maximos[1], 1.5f, 1, 15, -15, 90);
            creaLabel(15, maximos[2], 3, 2, -15, .2f, 20);
            creaLabel(-15, maximos[0], 3, 3, .2f, -15, 135);

            //Se crean las lineas auxiliares a la lectura, en forma de cuadriculado
            creaGrid(1, .2f,14.8f, 0,90, 0, 15, 1.5f);
            creaGrid(1, .2f, 0,14.8f, 90, 90, 15, 1.5f);
            creaGrid(0, -15f, 14.8f, 7.7f, 0, 0, 7.5f, 3);
            creaGrid(0, -15f, 0, .2f, 90, 90, 15f, 3);
            creaGrid(2, -15f, .2f, 0, 90, 0, 15f, 3);
            creaGrid(2, -15f, 7.7f, 14.8f, 0, 0, 7.5f, 3);

        }

        //Crea una columna que sirve de eje
        private void creaEjesY(float x, float y, float z, float altura)
        {
            //Se crea un cilidro, se posiciona, cambia de escala, color y se asigna un nombre
            GameObject ejeY = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            ejeY.transform.localPosition = new Vector3(x, y, z);
            ejeY.transform.localScale = new Vector3(.1f, altura, 0.1f);
            ejeY.GetComponent<Renderer>().material.color = Color.black;
            ejeY.gameObject.name = "EJE Y1";
        }

        //Crea las etiquetas en cada division de los ejes
        private void creaLabel(float inicio, float maximo, float separacion, int orientacion, float pos1, float pos2, float grados)
        {

            //Crearemos 10 diviciones en cada eje
            float incVal = maximo / 10;

            //Posicion inicial
            float pos = inicio;

            //Valor en la posicion inicial
            float valActual = 0;

            //Para cada uno de las divisiones
            for (int i=0; i<=10; i++)
            {
                //Creamos un objeto vacio de tipo texto
                GameObject txt = new GameObject();
                txt.AddComponent<TextMesh>();

                //Establecemos los tamaños, alineacion y color de texto
                txt.GetComponent<TextMesh>().characterSize = 0.05f;
                txt.GetComponent<TextMesh>().alignment = TextAlignment.Center;
                txt.GetComponent<TextMesh>().tabSize = 4;
                txt.GetComponent<TextMesh>().fontSize = 150;
                txt.GetComponent<TextMesh>().color = new Color(0, 0, 0, 252);

                //Se establece la rotacion que tendrá
                txt.transform.localRotation = Quaternion.Euler(0, grados, 0);

                //Dependiendo del eje la manera de presentar el texto y el ancla son diferentes
                txt.GetComponent<TextMesh>().text = orientacion == 0 || orientacion == 2 ? valActual.ToString("0,0.0", CultureInfo.InvariantCulture) + "-" : "-" + valActual.ToString("0,0.0", CultureInfo.InvariantCulture);
                txt.GetComponent<TextMesh>().anchor = orientacion == 0 || orientacion == 2 ? TextAnchor.MiddleRight : TextAnchor.MiddleLeft;
                
                //Dependiendo del tipo de eje en el que se encuentre la posicion es diferente
                switch (orientacion)
                {
                    case 0:
                        txt.transform.localPosition = new Vector3(pos1, pos, pos2);
                        txt.gameObject.name = "Y " + txt.GetComponent<TextMesh>().text;
                        break;

                    case 1:
                        txt.transform.localPosition = new Vector3(pos1, pos, pos2);
                        txt.gameObject.name = "Y " + txt.GetComponent<TextMesh>().text;
                        break;

                    case 2:
                        txt.transform.localPosition = new Vector3(pos1, pos2,pos);
                        txt.gameObject.name = "Z " + txt.GetComponent<TextMesh>().text;
                        break;
                        
                    case 3:
                        txt.transform.localPosition = new Vector3(pos,pos1, pos2);
                        txt.gameObject.name = "X " + txt.GetComponent<TextMesh>().text;
                        break;
                }

                //Actualizamos los valores para la siguiente etiqueta
                pos = orientacion == 2 ? pos - separacion : pos + separacion;
                valActual += incVal;
            }
        }

        //Crea las lineas auxiliares
        private void creaGrid(int tipo, float inicio, float pos1, float pos2, float rotX, float rotY, float largo, float separacion)
        {
            //Posicion de inicio
            float start =inicio ;

            //Para 10 divisiones
            for (int i = 0; i <= 10; i++)
            {
                //Creamos un cilindro, asignamos su rotacion y escala
                GameObject tmp = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
                tmp.transform.localScale = new Vector3(.1f, largo, .1f);
                tmp.transform.localRotation = Quaternion.Euler(rotX, rotY, 0);

                //Dependiendo de la orientacion, la posicion es diferente
                switch(tipo)
                {
                    case 0:
                        tmp.transform.position= new Vector3(pos1, pos2, start);
                        break;
                    case 1:
                        tmp.transform.position = new Vector3(pos1, start, pos2);
                        break;
                    case 2:
                        tmp.transform.position = new Vector3(start,pos1, pos2);
                        break;
                }

                //Se le asigna color al cilindro
                tmp.GetComponent<Renderer>().material.color = Color.black;

                //actualizamos la pocision para la siguiente linea
                start += separacion;

                //Añadimos un colider y un nombre para identificarlo
                tmp.GetComponent<CapsuleCollider>().enabled = false;
                tmp.gameObject.name = "Grid " + i.ToString();
            }
        }
    }
}
﻿
//---->Clase encargada de dar movimiento al usuario
using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{

    //Variables que usaremos en esta clase.
    public float velocidad = 500, maxSpeed = 20, desacelerar, desacelerarX, desacelerarZ, upDownVel = 100;
    //Objeto donde se almacena la referencia de la camara
    public GameObject camara;
    //Vector de desplazamiento
    private Vector2 horizontal;


    //Cada que exista un cambio en la fisica del Rigidbody...
    void FixedUpdate()
    {
        //Obtenemos el Rigidbody
        Rigidbody rb = GetComponent<Rigidbody>();

        //Al vector se le asigna la velocidad de movimiento del rigidbody
        horizontal = new Vector2(rb.velocity.x, rb.velocity.z);

        //Evitamos que la velocidad de desplazamiento sea mayor a la permitida
        if (horizontal.magnitude > maxSpeed)
        {
            horizontal = horizontal.normalized;
            horizontal *= maxSpeed;
        }

        //La nueva velocidad de desplazamiento es asignada al rigidbody
        rb.velocity = new Vector3(horizontal.x, 0, horizontal.y);

        //En caso de que ya no se desee mover el usuario, este detiende lentamente su desplazamiento
        if (Input.GetAxis("LAX") == 0 && Input.GetAxis("LAY") == 0)
        {
            rb.velocity = new Vector3(Mathf.SmoothDamp(rb.velocity.x, 0, ref desacelerarX, desacelerar), 0, Mathf.SmoothDamp(rb.velocity.z, 0, ref desacelerarZ, desacelerar));
        }

        //Igualamos la rotacion del usuario con la rotacion de la camara
        transform.rotation = Quaternion.Euler(0, TransferVariables.currentY, 0);

        //Se aplica una fuerza de desplazamiento al usuario al mover la palanca izquierda
        rb.AddRelativeForce(Input.GetAxis("LAX") * velocidad * Time.deltaTime, 0, Input.GetAxis("LAY") * velocidad * Time.deltaTime);

        //Si se presiona alguno de estos botones el usuario cambia su posicion en el eje Y, ya sea hasta arriba
        if (Input.GetButton("L1"))
        {
            rb.position += new Vector3(0, 0.05f, 0) * upDownVel;
        }
        //o hacia abajo
        if (Input.GetButton("R1"))
        {
            rb.position -= new Vector3(0, 0.05f, 0) * upDownVel;
        }
    }

    //En cada frame...
    void Update()
    {
        //Esperamos que el boton que nos envia al menu principal sea presionado
        if (Input.GetButton("Y"))
        {
            Application.LoadLevel("menu_principal");
        }
    }
}

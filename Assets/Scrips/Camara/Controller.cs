﻿using UnityEngine;

public class Controller : MonoBehaviour {

    public float inputDelay = 0.1f, forwardVel = 12, rotateVel = 100, upDownVel = 100;
    Quaternion targerRotation;
    Rigidbody rb;

    float forwardInput, turnInput;
    bool botonMenuPrincipal, botonMenuGrafica;
    public Quaternion TargetRotation
    {
        get { return targerRotation; }
    }

    void Start ()
    {
        targerRotation = transform.rotation;

        if (GetComponent <Rigidbody>())
        {
            rb = GetComponent<Rigidbody>();
        }

        botonMenuPrincipal = botonMenuGrafica= false;
        forwardInput = turnInput = 0;
	}

    void GetInput()
    {
        forwardInput = Input.GetAxis("LAY");
        turnInput = Input.GetAxis("LAX");
        botonMenuPrincipal = Input.GetButton("Y");
    }
	
	void Update ()
    {
        GetInput();
        Turn();
        if (botonMenuPrincipal)
        {
            Application.LoadLevel("menu_principal");
        }
    }

    void FixedUpdate()
    {
        Run();
        if (Input.GetButton("R1"))
        {
            rb.position += new Vector3(0, 0.05f, 0)*upDownVel;
        }
        if (Input.GetButton("L1"))
        {
            rb.position -= new Vector3(0, 0.05f, 0) * upDownVel;
        }
    }

    void Run()
    {
        rb.velocity= Mathf.Abs(forwardInput)> inputDelay ? transform.forward * forwardInput * forwardVel : Vector3.zero; 
    }

    void Turn()
    {
        if (Mathf.Abs(turnInput) > inputDelay)
        {
            targerRotation *= Quaternion.AngleAxis(rotateVel * turnInput * Time.deltaTime, Vector3.up);
        }
        transform.rotation = targerRotation;
    }
}
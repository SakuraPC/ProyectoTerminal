﻿using UnityEngine;

public class Camara : MonoBehaviour {
    public float sens = 1f, minY=-60, maxY=60;
    private float rotX = 0f, rotY = 0f;

	void Update () {
        rotY += Input.GetAxis("RAY") * sens;
        rotY = Mathf.Clamp(rotY, minY, maxY);
        transform.localEulerAngles = new Vector3(-rotY, rotX, 0);
	}
}

﻿
//---->Clase encargada de dar movimiento a la camara
using UnityEngine;
using System.Collections;

public class MouseLook : MonoBehaviour
{
    //Variables que guardan el grado de sencibilidad y la suavidad de movimiento
    public float lookSensitivity = 5, lookSmoothness = 0.1f;
    //Variables de posicionamiento en X-Y
    public float yRotation, xRotation, CurrentXRotation, CurrentYRotation=90, yRotationV, xRotationV;

    //Al iniciar el objeto...
    void Start()
    {
        //Asignamos el valor de rotacion inicial a las variables compartidas
        TransferVariables.currentY = 90;
    }

    //Por cada frame...
    void Update()
    {
        //Obtenemos los angulos de rotacion en base a la palanca derecha
        yRotation += Input.GetAxis("RAX") * lookSensitivity;
        xRotation -= Input.GetAxis("RAY") * lookSensitivity;

        //Nos cercioramos que la rotacion en X este dentro del rango permitido
        xRotation = Mathf.Clamp(xRotation, -80, 100);

        //Calculamos la nueva posicion de rotacion en ambos ejes
        CurrentXRotation = Mathf.SmoothDamp(CurrentXRotation, xRotation, ref xRotationV, lookSmoothness);
        CurrentYRotation = Mathf.SmoothDamp(CurrentYRotation, yRotation, ref yRotationV, lookSmoothness);

        //Asignamos las nuevas posiciones de rotacion a las variables compartidas
        TransferVariables.currentY = CurrentYRotation;
        TransferVariables.currentX = CurrentXRotation;

        //Asignamos a la rotacion actual su nueva posicion
        transform.rotation = Quaternion.Euler(xRotation, yRotation, 0);
    }
}
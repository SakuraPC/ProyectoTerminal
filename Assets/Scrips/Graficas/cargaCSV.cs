﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class cargaCSV : MonoBehaviour {

    string[,] Datos;
    public cargaCSV(string ruta)
    {
        List<string[]> values = new List<string[]>();
        var reader = new StreamReader(File.OpenRead(ruta));
        while (!reader.EndOfStream)
        {
            string line = reader.ReadLine();
            values.Add(line.Split(','));
        }

        Datos = CreateRectangularArray(values);
    }

    static T[,] CreateRectangularArray<T>(IList<T[]> arrays)
    {
        int minorLength = arrays[0].Length;
        T[,] ret = new T[arrays.Count, minorLength];
        for (int i = 0; i < arrays.Count; i++)
        {
            var array = arrays[i];
            if (array.Length != minorLength)
            {
                throw new System.ArgumentException
                    ("All arrays must be the same length");
            }
            for (int j = 0; j < minorLength; j++)
            {
                ret[i, j] = array[j];
            }
        }
        return ret;
    }

    public string[,] getDatos()
    {
        return Datos;
    }
}

﻿
//---->Clase encargada donde se guardan funciones recurrentes

using UnityEngine;
using System.Collections;

public class Funciones{

    public Funciones()
    { }
    //Regresa el valor mas proximo al limite
    public float limite (float valor)
    {
        float i = 1;
        while (i < valor)
        { i *= 10;}

        if (i - valor > i / 2)
        { i /= 2; }

        return i;
    }

    //Regresa un array de colores
    public Color[] arrayColor(int variables)
    {
        Color[] aux = new Color[variables];
        for (int i = 0; i < variables; i++)
        {
            aux[i] = new Color(0, 0, 0, 0);
            aux[i].r = Random.value;
            aux[i].g = Random.value;
            aux[i].b = Random.value;
            aux[i].a = 1.0f;
        }
        return aux;
    }

    //Regresa un color aleatorio
    public Color randomColor()
    {
        Color aux = new Color(0, 0, 0, 0);
        aux.r = Random.value;
        aux.g = Random.value;
        aux.b = Random.value;
        aux.a = 1.0f;
        return aux;
    }


    //Etiquetas para Superficie y Barras
    public GameObject creaEtiqueta(string titulo, float px, float pz, float rx, float ry, int r, int g, int b, int tipo)
    {
        GameObject txt = new GameObject();
        txt.AddComponent<TextMesh>();
        txt.GetComponent<TextMesh>().text = tipo == 0 ? "-" + titulo : titulo + "-";
        txt.GetComponent<TextMesh>().characterSize = 0.05f;
        txt.GetComponent<TextMesh>().anchor = tipo == 0 ? TextAnchor.MiddleLeft : TextAnchor.MiddleRight;
        txt.GetComponent<TextMesh>().alignment = TextAlignment.Center;
        txt.GetComponent<TextMesh>().tabSize = 4;
        txt.GetComponent<TextMesh>().fontSize = 100;
        txt.gameObject.name = titulo;
        txt.transform.localPosition = new Vector3(px, 0.5f, pz);
        txt.GetComponent<TextMesh>().color = new Color(r, g, b, 252);
        txt.transform.localRotation = Quaternion.Euler(rx, ry, 0);
        return txt;
    }

}
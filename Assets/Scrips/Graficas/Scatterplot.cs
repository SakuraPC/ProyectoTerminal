﻿
//---->Clase encargada de construir el tipo de grafica de puntos

using UnityEngine;
using System.Collections;

namespace Puntos
{
    public class Scatter
    {
        //Numero de puntos a graficar
        private int variablesNumero;
        //Lista donde guardamos los objetos punto
        private GameObject[] listaPuntos;
        //Variable de color
        private Color color;
        //Datos del CSV
        private string[,] arrayDatosCSV;
        //Lista con el nombre de las variables e indicadores
        private string[] listaVariables, listaIndicadores;
        //Datos del CSV numericos
        private float[,] datosFloat;
        //Lista con los valores maximos en las 3 dimencioens
        private float[] valoresMaximos;

        //Constructor
        public Scatter(string [,] datos)
        {
            //Obtenemos los datos
            arrayDatosCSV = datos;
        }

        public void main()
        {
            Funciones funciones = new Funciones();
            variablesNumero = arrayDatosCSV.GetLength(0) - 1;

            datosFloat = new float[variablesNumero, 3];
            listaVariables = new string[variablesNumero];
            listaIndicadores = new string[3];
            valoresMaximos = new float[3] { 0,0, 0 };
            
            //-Para cada dato
            for (int i = 0; i < variablesNumero; i++)
            {
                //-Para cada variable
                for(int j=0; j<3; j++)
                {
                    datosFloat[i,j] = float.Parse(arrayDatosCSV[i+1, j+1]);
                    //Obtengo los puntajes maximos
                    if (datosFloat[i,j]>valoresMaximos[j])
                    {
                        valoresMaximos[j] = datosFloat[i, j];
                    }
                }
            }
            //Nombre de las variables
            for (int i=0; i<variablesNumero; i++)
            {
                listaVariables[i] = arrayDatosCSV[i+1, 0];
            }
            
            //Nombre de los indicadores
            for (int i=0; i<3; i++)
            {
                listaIndicadores[i] = arrayDatosCSV[0, i+1];
            }

            //Calculamos los nuevos limites maximos
            for (int i=0; i<3; i++)
            {
                valoresMaximos[i] = funciones.limite(valoresMaximos[i]);
            }

            //Array donde estaran los objetos
            listaPuntos = new GameObject[variablesNumero];
            //Incializamos el objeto color
            color = new Color(0, 0, 0, 0);

            for (int i=0; i<variablesNumero;i++)
            {
                listaPuntos[i] = creaPunto(listaVariables[i], datosFloat[i, 0], datosFloat[i, 1], datosFloat[i, 2]);
                listaPuntos[i].GetComponent<Renderer>().material.color = funciones.randomColor();
            }

            for (int i=0; i<3; i++)
            {
                creaEtiqueta(i);
            }
        }
        private GameObject creaPunto(string nombreVar,  float X, float Y, float Z)
        {
            GameObject tmp = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            tmp.transform.localScale = new Vector3(.5f, .5f, .5f);
            tmp.transform.localPosition = new Vector3(posX(X), posY(Y)+.2f, posZ(Z));
            tmp.gameObject.name = nombreVar;
            return tmp;
        }

        //Realiza la conversion de la posicion en base al valor maximo de ese eje
        private float posX(float valor)
        {
            return ((valor * 30) / valoresMaximos[0]) - 15;
        }

        private float posY(float valor)
        {
            return (valor * 15) / valoresMaximos[1];
        }

        private float posZ(float valor)
        {
            return ((valor * -30) / valoresMaximos[2]) + 15;
        }

        public float[] getMaximos()
        {
            return valoresMaximos;
        }

        private void creaEtiqueta(int pos)
        {
            GameObject txt = new GameObject();
            txt.AddComponent<TextMesh>();
            txt.GetComponent<TextMesh>().characterSize = 0.05f;
            txt.GetComponent<TextMesh>().alignment = TextAlignment.Center;
            txt.GetComponent<TextMesh>().tabSize = 4;
            txt.GetComponent<TextMesh>().fontSize = 250;
            txt.GetComponent<TextMesh>().color = Color.blue;
            txt.GetComponent<TextMesh>().anchor = TextAnchor.MiddleCenter;
            txt.GetComponent<TextMesh>().text = listaIndicadores[pos];
            txt.gameObject.name = listaIndicadores[pos];

            switch (pos)
            {
                case 0:
                    txt.transform.localPosition = new Vector3(0, 0, -22);
                    txt.transform.localRotation = Quaternion.Euler(45,180, 0);
                    break;
                case 1:
                    txt.transform.localPosition = new Vector3(15,16, -10);
                    txt.transform.localRotation = Quaternion.Euler(0, 90, 0);
                    break;
                case 2:
                    txt.transform.localPosition = new Vector3(-22,0,0);
                    txt.transform.localRotation = Quaternion.Euler(45, 270, 0);
                    break;
            }
        }
    }
}
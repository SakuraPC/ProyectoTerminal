
//---->Clase encargada crear la grafica de barras

using System;
using UnityEngine;
namespace GraficaBarra
{
    public class Barras
    {

        //Variables globales
        //Altura maxima de las columnas
        private float alturaMaxima, valorMaximo;
        //- Numero de series, variables y el tipo de grafica
        private int seriesNumero, variablesNumero, tipoGrafica;
        //Array donde almacenamos las columnas
        private GameObject[,] columnasArray;
        //Array donde almacenamos los labels de las series y variables
        private GameObject[] listaEtiquetaVariables, listaEtiquetaSeries;
        //Vector donde se almacenan los colores
        private Color[] listaColores;
        //Datos del CSV
        private String[,] arrayDatosCSV;
        //Arrays con el nombre de las series y variables
        private String[] listaSeries, listaVariables;        
        //Datos ya formato float
        private float[,] datosFloat;      

        //Constructor
        public Barras(string[,] datos)
        {
            //Obtenemos el tipo de grafica de barra a dibujar
            tipoGrafica = TransferVariables.tipoBarra;
            //Cargamos los datos
            arrayDatosCSV = datos;
        }

        //FUNCION MAIN
        public void main()
        {
            Funciones funciones = new Funciones();
            valorMaximo = 0;

            //Definimos la altura maxima que tendran las columnas
            alturaMaxima = tipoGrafica == 1 ? 10f : 5f;
            
            //Obtenemos los datos en bruto y tomamos las dimenciones 
            //arrayDatosCSV = csv.getDatos();
            seriesNumero = arrayDatosCSV.GetLength(0) - 1;
            variablesNumero = arrayDatosCSV.GetLength(1) - 1;
            
            //Matriz y lista donde guardaremos 
            columnasArray = new GameObject[seriesNumero, variablesNumero];
            listaEtiquetaSeries = new GameObject[seriesNumero];
            listaEtiquetaVariables = new GameObject[variablesNumero];

            //Creamos un array donde estaran los datos numericos, los nombres de variables y series
            datosFloat = new float[seriesNumero, variablesNumero];
            listaSeries = new string[seriesNumero];
            listaVariables = new string[variablesNumero];

            //Definimos la distancia entre columnas
            float dvSerie = seriesNumero > 10 ? (float)19 / (seriesNumero - 1) : 2f; ;
            float dvVariable = variablesNumero>10?(float)19 / (variablesNumero - 1):2f;


            //Comenzamos el parseo de los datos en bruto
            //Obtenemos el nombre de las series
            for (int i=0; i<seriesNumero;i++)
            {
                listaSeries[i] = arrayDatosCSV[i+1, 0];
            }

            //Obtenemos el nombre de las variables
            for (int i = 0; i < variablesNumero; i++)
            {
                listaVariables[i] = arrayDatosCSV[0,i+1];
            }

            //Pasamos con los valores
            for (int i=0; i<seriesNumero; i++)
            {
                for (int j=0; j<variablesNumero; j++)
                {
                    datosFloat[i, j] = float.Parse(arrayDatosCSV[i+1, j+1]);
                     //Comprobamos que el valor sea el mas alto
                    if (datosFloat[i, j] > valorMaximo)
                    {
                        valorMaximo = datosFloat[i, j];
                    }
                }
            }
            
            //Actualizamos al limite mas alto
            valorMaximo = funciones.limite(valorMaximo);

            float posZ = 9.5f;
             listaColores = funciones.arrayColor(variablesNumero);

            //Comenzamos a llenar la matriz con las columnas
            for (int x = 0; x < seriesNumero; x++)
            {
                float posX = -9.5f;
                for (int z = 0; z < variablesNumero; z++)
                {
                    //Creamos el elemento
                    columnasArray[x, z] = creaColumna(posX, posZ,datosFloat[x,z]);

                    //Agregamos color
                    columnasArray[x, z].GetComponent<Renderer>().material.color = listaColores[z];

                    //Agregamos nombre para identificarlo
                    columnasArray[x, z].gameObject.name = listaSeries[x] + " " + listaVariables[z];

                    //Actualizamos posicion de las variables    
                    posX+= dvVariable;
                }

                //Actualizamos posicion de las series   
                posZ -= dvSerie;
            }

            for (int i = 0; i < seriesNumero; i++)
            {
                listaEtiquetaSeries[i] = funciones.creaEtiqueta(listaSeries[i],-10f, columnasArray[i, 0].transform.localPosition.z, 45, 10, 255, 0, 0, 1);
            }

            for (int i = 0; i < variablesNumero; i++)
            {
                listaEtiquetaVariables[i] = funciones.creaEtiqueta(listaVariables[i], columnasArray[0, i].transform.localPosition.x,-10f, 45, 90, 0, 0, 255, 0);
            }
        }

        private GameObject creaColumna(float x, float z, float valor)
        {
            //Creamos un objeto temporal
            GameObject tmp;

            //Basados en la altura maxima de nuestro escenario, calcularemos su equivalencia
            float nuevaAltura;

            float nuevaScala = posY(valor);
            //Dependiendo si es barras o cilindro se crea el objeto y asigna altura
            tmp = tipoGrafica == 1 ? GameObject.CreatePrimitive(PrimitiveType.Cube) : GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            nuevaAltura = tipoGrafica == 1 ? .2f + (nuevaScala / 2) : nuevaAltura = .2f+ nuevaScala;

            tmp.transform.localScale = new Vector3(0.5f, nuevaScala, 0.5f);
            tmp.transform.position = new Vector3(x, nuevaAltura, z);
            return tmp;
        }

        //Valor de la altura  de la barra en relacion al valor maximo
        private float posY(float valor)
        {
            return (valor * alturaMaxima) / valorMaximo;
        }

        //Regresa el valor maximo del conjunto de datos
        public float getMaximo()
        {
            return valorMaximo;
        }
    }
}
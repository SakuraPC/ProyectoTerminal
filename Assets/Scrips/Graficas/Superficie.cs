﻿
//---->Clase encargada de construir el tipo de grafica de superficie

using System;
using UnityEngine;
namespace GraficaBarra
{
    public class Superficie
    {

        //Variables globales
        //Altura maxima de las columnas
        private float alturaMaxima, valorMaximo;
        //- Numero de series, variables y el tipo de grafica
        private int seriesNumero, variablesNumero, tipoSuperficie;
        //Array donde almacenamos las columnas
        private GameObject[,] verticesArray;
        //Array donde almacenamos los labels de las series y variables
        private GameObject[] listaEtiquetaVariables, listaEtiquetaSeries;
        //Vector donde se almacenan los colores
        private Color[] listaColores;
        //Datos del CSV
        private String[,] arrayDatosCSV;
        //Arrays con el nombre de las series y variables
        private String[] listaSeries, listaVariables;
        //Datos ya formato float
        private float[,] datosFloat;

        private float sizeVertices;
        //Constructor
        public Superficie(string[,] datos)
        {
            arrayDatosCSV = datos;
            tipoSuperficie = TransferVariables.tipoSuperficie;
        }

        //FUNCION MAIN
        public void main()
        {
            Funciones funciones = new Funciones();
            valorMaximo = 0;
            //Definimos la altura maxima que tendran los puntos
            alturaMaxima = 10f;

            sizeVertices = TransferVariables.tipoSuperficie == 1 ? 0.15f : 0.001f;

            //Obtenemos los datos en bruto y tomamos las dimenciones 
            seriesNumero = arrayDatosCSV.GetLength(0) - 1;
            variablesNumero = arrayDatosCSV.GetLength(1) - 1;

            verticesArray = new GameObject[seriesNumero, variablesNumero];
            listaEtiquetaSeries = new GameObject[seriesNumero];
            listaEtiquetaVariables = new GameObject[variablesNumero];

            //Creamos un array donde estaran los datos numericos, los nombres de variables y series
            datosFloat = new float[seriesNumero, variablesNumero];
            listaSeries = new string[seriesNumero];
            listaVariables = new string[variablesNumero];

            //Definimos la distancia entre columnas
            float dvSerie = seriesNumero > 10 ? (float)19 / (seriesNumero - 1) : 2f; ;
            float dvVariable = variablesNumero > 10 ? (float)19 / (variablesNumero - 1) : 2f;

            //Comenzamos el parseo de los datos en bruto
            //Obtenemos el nombre de las series
            for (int i = 0; i < seriesNumero; i++)
            {
                listaSeries[i] = arrayDatosCSV[i + 1, 0];
            }

            //Obtenemos el nombre de las variables
            for (int i = 0; i < variablesNumero; i++)
            {
                listaVariables[i] = arrayDatosCSV[0, i + 1];
            }

            //Pasamos con los valores
            for (int i = 0; i < seriesNumero; i++)
            {
                for (int j = 0; j < variablesNumero; j++)
                {
                    datosFloat[i, j] = float.Parse(arrayDatosCSV[i + 1, j + 1]);
                    //Comprobamos que el valor sea el mas alto
                    if (datosFloat[i, j] > valorMaximo)
                    {
                        valorMaximo = datosFloat[i, j];
                    }
                }
            }

            //Actualizamos al limite mas alto
            valorMaximo = funciones.limite(valorMaximo);

            float posZ = 9.5f;
            listaColores = funciones.arrayColor(variablesNumero);

            //Creamos los vertices de referencia
            for (int x = 0; x < seriesNumero; x++)
            {
                float posX = -9.5f;
                for (int z = 0; z < variablesNumero; z++)
                {
                    //Creamos el elemento
                    verticesArray[x, z] = creaVertice(posX, posZ, datosFloat[x, z], sizeVertices);

                    //Agregamos color
                    verticesArray[x, z].GetComponent<Renderer>().material.color = listaColores[z];

                    //Agregamos nombre para identificarlo
                    verticesArray[x, z].gameObject.name = listaSeries[x] + " " + listaVariables[z];

                    //Actualizamos posicion de las variables    
                    posX += dvVariable;
                }

                //Actualizamos posicion de las series   
                posZ -= dvSerie;
            }

            //Creamos las etiquetas
            for (int i = 0; i < seriesNumero; i++)
            {
                listaEtiquetaSeries[i] = funciones.creaEtiqueta(listaSeries[i], -10f, verticesArray[i, 0].transform.localPosition.z, 45, 10, 255, 0, 0, 1);
            }

            for (int i = 0; i < variablesNumero; i++)
            {
                listaEtiquetaVariables[i] = funciones.creaEtiqueta(listaVariables[i], verticesArray[0, i].transform.localPosition.x, -10f, 45, 90, 0, 0, 255, 0);
            }

            //Dependiendo del tipo de grafica se ejecuta la funcion correspondiente
            switch(tipoSuperficie)
            {
                case 1:
                    malla();
                    break;
                case 2:
                    poligonos();
                    break;
            }
        }

        private void malla()
        {
            //Creamos las aristas
            for (int i = 0; i < seriesNumero; i++)
            {
                for (int j = 0; j < variablesNumero; j++)
                {
                    if (j < variablesNumero - 1)
                    {
                        creaArista(j, 1, verticesArray[i, j].transform.localPosition.x, verticesArray[i, j + 1].transform.localPosition.x, verticesArray[i, j].transform.localPosition.y, verticesArray[i, j + 1].transform.localPosition.y, verticesArray[i, j].transform.localPosition.z);
                    }

                    if (i < seriesNumero - 1)
                    {
                        creaArista(j, 2, verticesArray[i, j].transform.localPosition.z, verticesArray[i + 1, j].transform.localPosition.z, verticesArray[i, j].transform.localPosition.y, verticesArray[i + 1, j].transform.localPosition.y, verticesArray[i, j].transform.localPosition.x);
                    }
                }
            }
        }

        private void poligonos()
        {
            //Creamos los poligonos
            for (int i = 0; i < seriesNumero-1; i++)
            {
                for (int j = 0; j < variablesNumero-1; j++)
                {
                    creaPoligono(j,
                        verticesArray[i, j].transform.localPosition.x, verticesArray[i + 1, j].transform.localPosition.x, verticesArray[i, j+1].transform.localPosition.x, verticesArray[i+1, j+1].transform.localPosition.x,
                        verticesArray[i, j].transform.localPosition.y, verticesArray[i+ 1, j].transform.localPosition.y, verticesArray[i, j+1].transform.localPosition.y, verticesArray[i+1, j+1].transform.localPosition.y,
                        verticesArray[i, j].transform.localPosition.z, verticesArray[i+ 1, j].transform.localPosition.z, verticesArray[i, j+1].transform.localPosition.z,verticesArray[i+1, j + 1].transform.localPosition.z);
                }
            }
        }

        private GameObject creaVertice(float x, float z, float valor, float size)
        {
            //Creamos un objeto temporal
            GameObject tmp;

            float nuevaAltura = posY(valor)+.2f;
            //Dependiendo si es barras o cilindro se crea el objeto y asigna altura
            tmp = GameObject.CreatePrimitive(PrimitiveType.Sphere);

            tmp.transform.localScale = new Vector3(size,size,size);
            tmp.transform.position = new Vector3(x, nuevaAltura, z);
            return tmp;
        }

        private void creaArista(int iterador, int orientacion, float base1, float base2, float altura1, float altura2, float linea)
        {
            float distancia =(float) Math.Sqrt(Math.Pow(base2 - base1, 2) + Math.Pow(altura2 - altura1, 2));
            double angulo = Math.Atan((altura2 - altura1) / (base2 - base1));
            angulo = (angulo * (180 / Math.PI));

            float nBase = (base1 + base2) / 2;
            float nAltura = (altura1 + altura2) / 2;

            //Si es 1 representa vertical, si es 2 significa horizontal
            float nAngulo = orientacion == 1 ? (float)(90 + angulo) : (float)(90 - angulo);

            GameObject tmp = GameObject.CreatePrimitive(PrimitiveType.Cube);
            tmp.transform.localPosition = orientacion==1? new Vector3(nBase, nAltura, linea): new Vector3(linea, nAltura, nBase);
            tmp.transform.localRotation =orientacion==1? Quaternion.Euler(0, 0, nAngulo): Quaternion.Euler(nAngulo, 0, 0);
            tmp.transform.localScale = new Vector3(0.05f, distancia, .05f);
            tmp.GetComponent<Renderer>().material.color = listaColores[iterador];

        }

        private void creaPoligono(int iterador, float x1, float x2, float x3, float x4, float y1, float y2, float y3, float y4, float z1, float z2, float z3, float z4)
        {
            GameObject tmp = new GameObject();
            tmp.AddComponent<MeshFilter>();
            tmp.AddComponent<MeshRenderer>();
            Mesh mesh = new Mesh();
            tmp.GetComponent<MeshFilter>().mesh = mesh;
            Vector3[] vertices = new Vector3[4]
            {
                new Vector3(x1,y1,z1), new Vector3(x2,y2,z2), new Vector3(x3,y3,z3), new Vector3(x4,y4,z4)
            };

            int[] tri = new int[6];
            tri[0] = 0;
            tri[1] = 2;
            tri[2] = 1;

            tri[3] = 1;
            tri[4] = 2;
            tri[5] = 3;

            mesh.vertices = vertices;
            mesh.triangles = tri;
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            mesh.Optimize();
            tmp.GetComponent<Renderer>().material.color = listaColores[iterador];
        }
        private float posY(float valor)
        {
            return (valor * alturaMaxima) / valorMaximo;
        }

        public float getMaximo()
        {
            return valorMaximo;
        }
    }
}
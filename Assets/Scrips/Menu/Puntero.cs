﻿
//---->Clase encargada de dar movimiento al cursor en el menú principal
using UnityEngine;

public class Puntero : MonoBehaviour {
    //Limites en los dos ejes por los cuales el cursor se moverá
    public float xMin, xMax, yMin, yMax;

    //Rigidbody del cursor
    private Rigidbody rb;

    //Al iniciar el objeto...
    void Start () {
        //Obtenemos el rigidbody
        rb = GetComponent<Rigidbody>();
    }

    //Cada que exista un cambio en la fisica del Rigidbody...
    void FixedUpdate()
    {
        //Obtenemos los valores de posicionamiento al mover la palanca izquierda
        float h = Input.GetAxis("LAX");
        float v = Input.GetAxis("LAY");

        //Asignamos a un vector las posiciones a cursor
        Vector3 vector = new Vector3(h, v, -1);

        //Velocidad de movimiento
        rb.velocity = vector * 5;

        //Cambiamos la ubicacion del cursor de acuerdo al vector de posicion y a los limites definidos
        rb.position = new Vector3(Mathf.Clamp(rb.position.x, xMin, xMax), Mathf.Clamp(rb.position.y, yMin, yMax), -1);
    }
}
﻿
//---->Clase encargada del control del menu de seleccion principal
using System.Collections;
using UnityEngine;

public class MenuOpciones : MonoBehaviour {
    private int op = 0;
    private string[,] graficas; 
    private string idSeleccionado="";
    private WWW w;

    //Al iniciar el objeto...
    void Start () {
        //Seteamos nuestras variables en 0
        TransferVariables.tipoBarra = 0;
        TransferVariables.tipoSuperficie = 0;

        //Iniciamos la construccion del menú
        main();
    }

    //Por cada frame...
    void Update () {

        //Esperamos a que el boton de seleccion sea presionado y que el cursor este sobre una opcion
        if (Input.GetButtonDown ("A") && idSeleccionado!="") {
            //Obtenemos el indice de la grafica en la matriz de datos
            int a=indice (idSeleccionado);

            //Guardamos el id de la grafica 
            TransferVariables.idGrafica = graficas [a, 0];

            //Dependiendo del tipo de grafica seleccinada se carga el escenario adecuado y se guarda el estilo de grafica, excepto para el de puntos
            switch (graficas [a, 1])
            {
            case "1":
                TransferVariables.tipoBarra = 1;
                Application.LoadLevel ("barras");
                break;
            case "2":
                TransferVariables.tipoBarra = 2;
                Application.LoadLevel ("barras");
                break;
            case "3":
                Application.LoadLevel ("scatter");
                break;
            case "4":
                TransferVariables.tipoSuperficie = 1;
                Application.LoadLevel("superficie");
                break;
            case "5":
                    TransferVariables.tipoSuperficie = 2;
                    Application.LoadLevel("superficie");
                    break;
            }
        }

    }

    //Comenzamos la construccion del menú
    public void main(){
        //Iniciamos la peticion al servidor
        StartCoroutine(descarga());
    }

    //Si un trigger de las opciones se activa
    void OnTriggerEnter(Collider obj){
        //Obtenemos el id de la opcion
        idSeleccionado = obj.gameObject.name;
    }
    
    //Si no hay trigger actvo
    void OnTriggerExit(Collider obj){
        //Reseteamos el id
        idSeleccionado="";
    }
        
    //Realiza la peticion al servidor
    IEnumerator descarga(){
        //Crea un objeto con la respuesta obtenida por el servidor en la ruta especificada
        w = new WWW(TransferVariables.servidor+ "all_graphics");
        yield return w;
        yield return new WaitForSeconds(1f);

        //Procesamos la respuesta recibida
        nombreGraficas(w.text);
    }

    //Se crea el listado con las graficas disponibles
    void nombreGraficas(string json){
        //Convertimos la respuesta en un objeto JSON
        JSONObject jo = new JSONObject(json);

        //Creamos una matriz donde guardaremos la informacion del JSON
        graficas= new string[jo.Count,jo[0].Count];

        //Recorremos el JSON para guardar los elementos del JSON en la matriz
        for (int i = 0; i < jo.Count; i++) {
            for (int j = 0; j < jo [0].Count; j++) {
                graficas [i, j] = jo [i] [j].ToString().Replace("\"", "");
            }
        }

        //Asignamos la posicion en pantalla del primer elemento de la lista
        float posX = -6f, posY = 5f;

        //Para cada renglon
        for (int i = 0; i < graficas.GetLength(0); i++) {
            //Creamos una etiqueta de submenú con el nombre de la grafica
            creaSubmenu(posX+.5f, posY, graficas[i,3], 100,Color.black, graficas[i, 0]);
            
            //Creamos una etiqueta de submenú con el nombre del tipo de la grafica
            creaSubmenu(posX + .5f, posY-.5f, graficas[i, 5], 75, Color.red, "S"+i);

            //Creamos el trigger de la opcion
            creaTrigger(posX, posY, graficas [i, 0]);

            //Preparamos la posicion del siguiente elemento de la lista
            posY-=2f;
        }
    }

    //Crea etiquetas en el plano del menú
    private void creaSubmenu(float posX, float posY, string titulo, int size, Color color, string ID)
    {
        //Crea un objeto vacio
        GameObject txt = new GameObject();

        //Le añade el componente de texto
        txt.AddComponent<TextMesh>();

        //Se declara el contenido del texto, tamaño del caracter, ancla, alineacion, tamaño de tabulador y de texto
        txt.GetComponent<TextMesh>().text = titulo;
        txt.GetComponent<TextMesh>().characterSize = 0.05f;
        txt.GetComponent<TextMesh>().anchor = TextAnchor.MiddleLeft;
        txt.GetComponent<TextMesh>().alignment = TextAlignment.Left;
        txt.GetComponent<TextMesh>().tabSize = 4;
        txt.GetComponent<TextMesh>().fontSize = size;

        //El texto es colocado en su posicion, se le asigna color y nombre al objeto
        txt.transform.localPosition = new Vector3(posX, posY, 0);
        txt.GetComponent<TextMesh>().color = color;
        txt.gameObject.name =ID;
        txt.AddComponent<BoxCollider>();
        txt.GetComponent<BoxCollider>().isTrigger = true;
        txt.GetComponent<BoxCollider>().size = new Vector3(10, 1, 3);

    }

    //Crea el trigger o disparador
    private void creaTrigger(float posX, float posY, string titulo)
    {
        //Crea un cubo
        GameObject tmp = GameObject.CreatePrimitive (PrimitiveType.Cube);
        
        //El tamaño y posicion son asignados
        tmp.transform.localScale = new Vector3 (.5f, .5f, 1);
        tmp.transform.localPosition = new Vector3 (posX, posY, 0);
        
        //Se le coloca un titulo y color
        tmp.gameObject.name = titulo;
        tmp.GetComponent<Renderer> ().material.color = Color.green;
        
        //Se declara que el objeto es un trigger y se declara el tamaño del collider
        tmp.GetComponent<BoxCollider> ().isTrigger = true;
        tmp.GetComponent<BoxCollider> ().size = new Vector3 (10, 2.5f, 3);
    }

    //Regresa el indice de la grafica perteneciente al renglon dentro de la matriz
    private int indice(string id)
    {
        for (int i = 0; i < graficas.GetLength (0); i++) {
            for (int j = 0; j < graficas.GetLength (1); j++) {
                if (graficas[i,j].Equals(id)){
                        return i;
                    }
            }
        }
        return -1;
    }
}
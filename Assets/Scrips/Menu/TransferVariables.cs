﻿
//---->Clase donde almacenamos variables que comparten informacion entre dos clases

public class TransferVariables
{
    // Variables para identificar el tipo de grafica
    public static int tipoBarra = 0;
    public static int tipoSuperficie = 0;

    //Variable que guarda el ID de la grafica
    public static string idGrafica="";

    //Datos de la grafica en bruto 
    public static string[,] rawGrafica;

    //Direccion del servidor
    //public static string servidor= "http://192.168.1.64:3000/";
    //public static string servidor= "http://192.168.43.53:3000/";
    public static string servidor= "http://localhost:3000/";

    //Posiciones de la camara
    public static float currentY, currentX;
}